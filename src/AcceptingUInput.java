
import java.util.Scanner;

public class AcceptingUInput {

	public static void main(String[] args) {
	
	Scanner user_input = new Scanner(System.in);
	
	
	String first_name;
	System.out.print("What is your first name:");
	first_name = user_input.next();
	
	
	String family_name;
	System.out.print("What is your family name:");
	family_name = user_input.next();
	
	String full_name;
	full_name = first_name+" "+family_name;
	
	String celeb_name;
	System.out.print("What is the last name of your favorite celeb:");
	celeb_name = user_input.next();
	
	
	System.out.println("Your name is "+full_name);
	System.out.println("Your fantasy name is "+first_name+" "+celeb_name+" And it will remain a fantasy :)");


	}
}
