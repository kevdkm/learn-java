
import javax.swing.JOptionPane;

public class RectangleArea {
	
	public static void main(String[] args) {
		
		String width;
		width = JOptionPane.showInputDialog("What is Rectangle Width","Rectangle Width");
				
		String height;
		height = JOptionPane.showInputDialog("What is Rectangle Height","Rectangle Height");
		
		float area = Float.parseFloat(width)*Float.parseFloat(height);
		
		String area_answer = "The area of your rectangle: " + area;
		
		JOptionPane.showMessageDialog(null,area_answer,"Area of Rectangle",JOptionPane.INFORMATION_MESSAGE);
		
		System.exit(0);
		
	}

}
