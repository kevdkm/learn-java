import javax.swing.JOptionPane;

public class JavaOptionPanes {
	
	public static void main(String[] args) {
		
		String first_name;
		first_name = JOptionPane.showInputDialog("First Name","Enter Your First Name");

		String family_name;
		family_name = JOptionPane.showInputDialog("Family Name","Enter Your Family Name");
	
		String full_name;
		full_name ="You are " + first_name + family_name;
				
		JOptionPane.showMessageDialog(null,full_name,"Name",JOptionPane.WARNING_MESSAGE);
		System.exit(0);
	}

	
}

/*
import javax.swing.JOptionPane;  -- This will setup the pop up boxes


public class JavaOptionPanes {  -- standard starting statement
	
	public static void main(String[] args) { -- standard statement
		
		String first_name; -- Defining datatype for variables
		first_name = JOptionPane.showInputDialog("First Name","Enter Your First Name"); -- first_name = JOptionPane.showInputDialog # prompt a box to ask the following
		 																				-- "First Name" # outside dialogue
		 																				-- ,"Enter Your First Name" # highlighted text in the text field

		String family_name;
		family_name = JOptionPane.showInputDialog("Family Name","Enter Your Family Name");
	
		String full_name;																
		full_name ="You are " + first_name + family_name;
		
		JOptionPane.showMessageDialog(null,full_name,"Name",JOptionPane.WARNING_MESSAGE); -- JOptionPane.showMessageDialog # prompt a box that will display inputs
																						  -- (null # This is a java keyword and just means that the message box is not associated with anything else in the programme.
																						  -- full_name # the variable that will be displayed
																						  -- "Name" # the outside dialogue in the box
																						  -- JOptionPane.WARNING_MESSAGE # is the box and icon to be defined
																						  -- The following can be used to change the icon in the popup box.
																									INFORMATION
																									ERROR_MESSAGE
																									PLAIN_MESSAGE
																									QUESTION_MESSAGE
																									WARNING_MESSAGE
		
		System.exit(0); --  programme exits. But it also tidies up for us, removing all the created objects from memory.
	}

	

}

*/